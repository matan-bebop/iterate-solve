batchload("util/linear.mac")$

linsolve1(eq, x) := block(
/* Solves a linear equation eq for x. 

   If eq is not linear, throws an error.

   60-70% faster in GCL than linsolve in the example from the bug report #4048.
   Does not expand denominators. Works well with orderless in GCL (see the 
   bug #4048 in Maxima). 
*/
	[eq_prop_x, eq_const],

	if not linear(eq, x) then
		error("Non-linear expression in eq"),

	eq_prop_x : ratcoef(eq, x), 
	eq_const : subst([x = 0], eq),

	eq_prop_x : eq_prop_x - rhs(eq_prop_x),
	eq_const : eq_const - lhs(eq_const),
	
	x = rhs(eq_const) / lhs(eq_prop_x))$
